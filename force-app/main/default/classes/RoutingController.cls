public with sharing class RoutingController {
    /**
     * An empty constructor for the page
     */
    public RoutingController() {}

    public static String endPoint = 'https://public-api.mapanything.io/mare/routing/sync';

    public static String APIToken {
        get {
            if (APIToken == null) {
                APIToken = 'soiltWpxYP9ddhbnZN3KC32uLu6pIFFI6Hdkjp45';
            }
            return APIToken;
        }
        set;
    }

    @RemoteAction
    public static Map<String, Object> getOptimizedRoute(String options) {
        try {
            if(String.isBlank(options)) {
                throw new RoutingException('No options were passed to the callout.');
            }
            return makeHttpCallOut(options, 'POST');
        } catch (Exception ex) {
            return new Map<String, Object> {
                'success' => false,
                'message' => ex.getMessage()
            };
        }
    }

    public static Map<String, Object> makeHttpCallOut(String options, String method) {

        String token = APIToken;

        if (String.isBlank(token)) {
            throw new RoutingException('You have not provided an API Token.');
        }
        if (String.isBlank(endPoint)) {
            throw new RoutingException('No endpoint was provided. An endpoint for the API is required in order to make a callout.');
        }

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setHeader('Content-Type', 'application/json');
        req.setMethod(method);
        req.setTimeout(60000);
        req.setHeader('x-api-key', token);

        if(method != 'GET') {
            req.setBody(options);
        }

        Http http = new Http();
        HttpResponse res = http.send(req);
        return (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
    }

    public class RoutingException extends Exception {}
    
}
