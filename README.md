![SF-Maps Logo](./img/sfmapslogo.svg)

## Tourist Routing App (Routing Engine Demo)

Clone this repo and cd into the root of the project folder:

    git clone https://gitlab.com/ma-msorto/df19-trailhead-zone
    
    cd df19-trailhead-zone
    
## Create a Scratch Org

For this demo you will need to create a [Scratch Org](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_scratch_orgs.htm) using the **Salesforce CLI** - [Here's a link to installation steps in case you don't have it](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_install_cli.htm). If you don't have access to a Salesforce Org with **Dev Hub mode** enabled, you can [get a Developer Edition Org here](https://developer.salesforce.com/signup) and follow these steps to [set up Dev Hub mode in your org](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_enable_devhub.htm).

Authorize your Dev Hub org:

    sfdx force:auth:web:login -d -a "DevHub"

OR if you already have an authorized Dev Hub, set it as the default:

    sfdx force:config:set defaultdevhubusername=<username|alias>

After authenticating into your Dev Hub org, create a scratch org:

    sfdx force:org:create -s -f config/project-scratch-def.json -d 30 -a "MyScratchOrg"

OR If you want to use an existing scratch org, set it as the default:

    sfdx force:config:set defaultusername=<username|alias>

Push this repo's source:

    sfdx force:source:push
    
Apply the PermissionSet that gives you access to the "Tourist Routing App" in App Launcher by running this Apex script:

	sfdx force:apex:execute -f ../df19-trailhead-zone/config/set-permissions.apex

## Launch the Tourist App

    sfdx force:org:open --path one/one.app#/alohaRedirect/apex/TouristApp 

![App Screenshot](./img/screenshot.png)
