@isTest (SeeAllData = false)
public class RoutingController_Test {
    
    static String options = '{"add_stats": true,"locations": [{"location_id": "L33.65898::-84.01731","latitude": 33.65898,"longitude": -84.01731},{"location_id": "L33.78842::-84.08634","latitude": 33.78842,"longitude": -84.08634},{"location_id": "L33.7967299::-84.1769","latitude": 33.7967299,"longitude": -84.1769},{"location_id": "L33.8128084::-84.1502201","latitude": 33.8128084,"longitude": -84.1502201}],"orders": [{"order_id": "id-1","location_id": "L33.65898::-84.01731","duration": 1800,"appointments": [{"appointment_start": "2019-03-19T11:04:25Z","appointment_end": "2019-03-19T11:34:25Z"}],"priority": 1},{"order_id": "id-2","location_id": "L33.78842::-84.08634","duration": 1800,"priority": 1},{"order_id": "id-3","location_id": "L33.7967299::-84.1769","duration": 1800,"priority": 1},{"order_id": "id-4","location_id": "L33.8128084::-84.1502201","duration": 1800,"priority": 1}],"vehicles": [{"type": "car","vehicle_id": "vehicle-001","shifts": [{"shift_id": "S::first::last","start_location_id": "L33.65898::-84.01731","end_location_id": "L33.65898::-84.01731","shift_start": "2019-03-18T15:37:45Z","shift_end": "2019-03-19T19:24:25Z"}]}],"constraints": [{"constraint_name": "Core-Schedule-SingleDay::Travel_Time","constraint_type": "Travel_Time","max_travel_time_seconds": 0,"penalty_per_violation": 1,"violation_increment": 1},{"constraint_name": "Core-Schedule-SingleDay::Visit_Range","constraint_type": "Visit_Range","penalty_per_violation": 10000,"violation_increment": 1},{"constraint_name": "Core-Schedule-SingleDay::Order_Priority","constraint_type": "Order_Priority","penalty_per_violation": 5000,"violation_increment": 1},{"constraint_name": "Core-Schedule-SingleDay::Scheduled_Appointments","constraint_type": "Scheduled_Appointment","penalty_per_violation": 10000,"order_ids": ["id-1"],"violation_increment": 1000,"hard_constraint": true}]}';

    @isTest
    public static void test_getOptimizedRoute_NullOptions() {

        options = null;
        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(false, resp.get('success'));
        System.assertEquals('No options were passed to the callout.', resp.get('message'));
    }

    @isTest
    public static void test_getOptimizedRoute_EmptyOptions() {

        options = '';
        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(false, resp.get('success'));
        System.assertEquals('No options were passed to the callout.', resp.get('message'));
    }

    @isTest
    public static void test_getOptimizedRoute_Success() {
        // this just gets an instance to cover the constructor code
        RoutingController rc = new RoutingController();

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(MockHttpResponseGenerator.ResponseType.SUCCESS));

        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(true, resp.get('success'));
        System.assertEquals('General success message', resp.get('message'));
    }

    @isTest
    public static void test_getOptimizedRoute_Error() {

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(MockHttpResponseGenerator.ResponseType.ERROR));

        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(false, resp.get('success'));
        System.assertEquals('Generic Error Message For Test Methods', resp.get('message'));
    }

    @isTest
    public static void test_getOptimizedRoute_noToken() {

        RoutingController.APIToken = '';

        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(false, resp.get('success'));
        System.assertEquals('You have not provided an API Token.', resp.get('message'));
    }

    @isTest
    public static void test_getOptimizedRoute_noEndpoint() {

        RoutingController.endPoint = '';

        // test remote call
		Test.startTest();

            Map<String, Object> resp = RoutingController.getOptimizedRoute(options);
            System.debug('resp: ' + resp);

		Test.stopTest();

        System.assertEquals(false, resp.get('success'));
        System.assertEquals('No endpoint was provided. An endpoint for the API is required in order to make a callout.', resp.get('message'));
    }

}