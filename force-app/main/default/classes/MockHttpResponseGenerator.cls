public class MockHttpResponseGenerator implements HttpCalloutMock {
    public enum ResponseType {ERROR, NO_RESP, SUCCESS}
    private MockHttpResponseGenerator.ResponseType resType;

    public static String goodResp = '{"success":true,"message": "General success message"}';
    public static String errorResp = '{"success":false,"message": "Generic Error Message For Test Methods"}';
    
    public MockHttpResponseGenerator(MockHttpResponseGenerator.ResponseType resType) {
        this.resType = resType;
    }
    
    public HttpResponse respond(HttpRequest httpReq) {
        String requestEndpoint = httpReq.getEndpoint();
        
        HttpResponse httpRes = new HttpResponse();
        httpRes.setHeader('Content-Type', 'application/json');

        if(resType == MockHttpResponseGenerator.ResponseType.SUCCESS) {
            httpRes.setStatusCode(200);
            httpRes.setBody(goodResp);
        }
        else if(resType == MockHttpResponseGenerator.ResponseType.ERROR) {
            httpRes.setStatusCode(400);
            httpRes.setBody(errorResp);
        }
        
        return httpRes;
    }
}
